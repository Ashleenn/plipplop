﻿using UnityEngine;

public interface Noticeable
{
    void Notice();
    bool IsVisible();
    void SetVisible(bool value);
}
